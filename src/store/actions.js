//modal actions
export {
    closeModal,
    openModal
} from '../common/Modal/services/modalAction'

export {
    setLoading,
    clearLoading
}  from '../common/Loading/services/loadingAction'

export {
    setToasterState,
}  from '../common/Toaster/services/ToasterActions'

export {
    checkAuthentication,
    logoutUser
} from '../modules/Authentication/services/authenticationAction'
