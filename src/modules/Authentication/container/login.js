import React, {useState} from 'react';
import '../authentication.scss'
import Loading from "../../../common/Loading";
import {useDispatch, useSelector} from "react-redux";
import * as actions from '../services/authenticationAction'
import Toaster from "../../../common/Toaster/Toaster";

const Login = (props) => {
    const [user, setUsers] = useState({})
    const dispatch = useDispatch()
    const {loadingState} = useSelector(state => state.loadingReducer)
    const handleChange = (e) => {
        e.persist()
        setUsers({
            ...user,
            [e.target.name]: e.target.value
        })

    }

    const handleSubmit = () => {
       props.location.pathname === "/admin" ? dispatch(actions.loginAdmin(user)): dispatch(actions.loginUser(user))
    }

    return (
      <>
        <section className="login">
          <Toaster />
          <div action="" className={"login-form"}>
            <div className="title">
              Login as{" "}
              {props.location.pathname === "/admin" ? "Admin" : "User"}
            </div>
            <div className="form-group">
              <div className="input-group">
                <div className="input-box">
                  <input
                    type="email"
                    name={"email"}
                    placeholder={"Your email goes here"}
                    onChange={(e) => handleChange(e)}
                  />
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="input-group">
                <div className="input-box">
                  <input
                    type="password"
                    name={"password"}
                    placeholder={"Your password here"}
                    onChange={(e) => handleChange(e)}
                  />
                </div>
              </div>
            </div>
            <div className="form-group">
              <div
                className="btn primary d-flex justify-evenly align-center"
                onClick={() => handleSubmit()}
              >
                <div>Login</div>
                {loadingState === "login" && <Loading isButton={true} />}
              </div>
            </div>
          </div>
        </section>
      </>
    );
};

export default Login;


