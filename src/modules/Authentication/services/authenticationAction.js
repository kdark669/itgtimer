import * as actionType from './authenticationType'
import apolloClient from "../../../config/apolloclient";
import * as actions from '../../../store/actions'

import gql from "graphql-tag";

const authSuccess = (user) => {
    return {
        type: actionType.LOGIN_USER,
        user: user
    }
}

const authFail = () => {
    return {
        type: actionType.LOGOUT_USER
    }
}

const searchUser = (email)  => {
    return apolloClient.mutate(
        {
            mutation: gql`
                query MyQuery($email : String!) {
                    users(where: {email: {_eq: $email}}) {
                        email
                        id
                        password
                        user_type
                    }
                }
            `,
            variables: {
                email : email
            }
        }
    )
}

const checkPassword = (passwordEnter, originalPasword) => {
    if(originalPasword === passwordEnter){
        return true
    }else{
        return false
    }
}

export const checkAuthentication = () => (dispatch) => {
    try {
        dispatch(actions.setLoading('login'))
        let authUser = localStorage.getItem("user");
        console.log("authuser", authUser)
        if (authUser) {
            const userData = JSON.parse(authUser);
            dispatch(actions.clearLoading())
            dispatch(authSuccess(userData));
        } else {
            dispatch(actions.clearLoading())
            dispatch(authFail());
        }
    }catch (e) {

    }

}


export const loginAdmin = (user) =>  async dispatch => {
    try{
        console.log("subash", user)
        dispatch(actions.setLoading('login'))
        const existedUser = (await searchUser(user.email)).data.users[0]
        const isAdmin = existedUser.user_type === "admin";
        const isMatch = await checkPassword(user.password,existedUser.password)
        if (isMatch && isAdmin) {
          localStorage.setItem("user", JSON.stringify(existedUser.id));
          localStorage.setItem(
            "type",
            2
          );
          dispatch(
            actions.setToasterState(
              true,
              "Success",
              "Login Success",
              `Welcome ${user.email}`
            )
          );
          dispatch(authSuccess(user));
          dispatch(actions.clearLoading());
        } else {
          dispatch(
            actions.setToasterState(
              true,
              "Error",
              "Authentication Problem",
              "Login Failed"
            )
          );
          dispatch(actions.clearLoading());
          dispatch(authFail());
        }
    }catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Authentication Problem",
                "Login Failed"
            )
        )
        dispatch(actions.clearLoading())
        dispatch(authFail());
    }

}

export const loginUser = (user) =>  async dispatch => {
    try{
        console.log("subash", user)
        dispatch(actions.setLoading('login'))
        const existedUser = (await searchUser(user.email)).data.users[0]
        const isUser = existedUser.user_type === "user";
        const isMatch = await checkPassword(user.password,existedUser.password)
        if (isMatch && isUser) {
          localStorage.setItem("user", JSON.stringify(existedUser.id));
            localStorage.setItem("loginTime", Date.now());
            localStorage.setItem("type", 1);
          dispatch(
            actions.setToasterState(
              true,
              "Success",
              "Login Success",
              `Welcome ${user.email}`
            )
          );
          dispatch(authSuccess(user));
          dispatch(actions.clearLoading());
        } else {
          dispatch(
            actions.setToasterState(
              true,
              "Error",
              "Authentication Problem",
              "Login Failed"
            )
          );
          dispatch(actions.clearLoading());
          dispatch(authFail());
        }
    }catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Authentication Problem",
                "Login Failed"
            )
        )
        dispatch(actions.clearLoading())
        dispatch(authFail());
    }

}

export const logoutUser = () => dispatch => {
    try{
        dispatch(actions.setLoading())
        localStorage.removeItem("user");
        dispatch(authFail());
        dispatch(
          actions.setToasterState(
            true,
            "Success",
            "Authentication",
            "Logout Success"
          )
        );
    }catch (e) {
        dispatch(
            actions.setToasterState(
                true,
                "Error",
                "Authentication Fail",
                "Logout Failed"
            )
        )
        dispatch(actions.clearLoading())
    }
}

export const registerUser = (user) => dispatch => {
    //sign up user logic goes here  with dispatch of two action authSuccess or authFail
}
