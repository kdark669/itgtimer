import * as actionTypes from './authenticationType'

const initialState = {
    isLoggedIn: false,
    loginUser:{},
}

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.LOGIN_USER:
            return {
                ...state,
                loginUser: actions.user,
                isLoggedIn:true,
            };

        case actionTypes.LOGOUT_USER:
            return {
                ...state,
                loginUser: {},
                isLoggedIn:false
            }
        default:
            return state
    }
}
