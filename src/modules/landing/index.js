import React from 'react';
import {useDispatch} from "react-redux";
import * as actions from '../../store/actions'

const LandingLayout = props => {
    const dispatch = useDispatch()

    const handleLogout = () => {
        dispatch(actions.logoutUser())
    }

    return (
        <>
            <div className="btn primary" onClick={handleLogout}>
                Logout
            </div>
        </>
    );
};

export default LandingLayout;
