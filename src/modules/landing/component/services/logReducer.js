import * as actionTypes from './logType';

const initialState = {
  loading: false,
};

export default (state = initialState, actions) => {
  switch (actions.type) {
    case actionTypes.LOADING:
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
};
