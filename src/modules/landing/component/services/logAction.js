// import * as actionType from './logType';
import apolloClient from '../../../../config/apolloclient';
import gql from 'graphql-tag';
import * as actions from "../../../../store/actions";

export const addLog = (data) => async (dispatch) => {
  try {
    dispatch(actions.setLoading('log'));
    console.log(data);
     let x  = await apolloClient.mutate({
      mutation: gql`
      mutation MyMutation($title : String!, $total_time :Int!, $detail : String!, $special_note :String!) {
        insert_user_log(
          objects: {
            detail: $detail
            special_note: $special_note
            title: $title
            total_time: $total_time
            user_id: 1
          }
        ) {
          affected_rows
          returning {
            id
            title
          }
        }
      }
    `,
      variables: {
        title: data.title,
        total_time: data.duration,
        detail: data.details,
        special_note: "none"
      },
     });
    if (x) {
      console.log(x);
      dispatch(actions.setToasterState(
        true,
        "Success",
        "Log Added ",
        "Your work log is saved."
      )
      )
      dispatch(actions.clearLoading())
    }

  }
  catch (e) { 
       dispatch(
         actions.setToasterState(
           true,
           "Error",
           " Problem saving your log",
           "Logging Failed"
         )
       );
       dispatch(actions.clearLoading());
  }
};

export const TimeTracker = (data) => async (dispatch) => {
  try {
    dispatch(actions.setLoading("log"));
    console.log(data);
    let x = await apolloClient.mutate({
      mutation: gql`
        mutation MyMutation($timeTracked: Int!, $user_id: Int!) {
          insert_time_tracker(
            objects: { time_tracked: $timeTracked, user_id: $user_id }
          ) {
            affected_rows
            returning {
              time_tracked
              id
            }
          }
        }
      `,
      variables: {
        timeTracked: data,
        user_id: localStorage.getItem("user"),
      },
    });
    if (x) {
      console.log(x);
      dispatch(
        actions.setToasterState(true, "Success", "Log Added ", "Timer Stopped.")
      );
      dispatch(actions.clearLoading());
    }
  } catch (e) {
    dispatch(
      actions.setToasterState(
        true,
        "Error",
        " Problem saving your log",
        "Error in stopping time"
      )
    );
    dispatch(actions.clearLoading());
  }
};
