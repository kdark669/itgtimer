import React, { useState, useEffect} from 'react';
import AdminRoute from "./routes/adminRoute";
import AddUser from "./views/addUser";
import MessageUser from "./views/messageUser";
import Notifications from "./views/notifications";
import Performance from "./views/performance";
import MainDashboard from "./views/mainDashboard";

const AdminLayout = props => {
  const [selectedTab, setSelectedTab] = useState("Dashboard");
  const changeDisplay = (val) => { 
    console.log("addusers", val);
    setSelectedTab(val)
  }

  const handleLogout = () => {
    console.log("subash");
     localStorage.removeItem("user");
   };
  
    return (
      <>
        <div className="main-dash">
          <div className="sidebar">
            <div className="title">
              <span className="title-red">ITG </span>
              <span className="title-blue">&nbsp; Timer </span>
            </div>
            <div className="opt-row">
              <div
                className={
                  selectedTab === "Dashboard"
                    ? "selectedoptions"
                    : "options"
                }
                onClick={() => changeDisplay("Dashboard")}
              >
                Dashboard
              </div>
              <div
                className={
                  selectedTab === "" ? "selectedoptions" : "options"
                }
                onClick={() => changeDisplay("Add Users")}
              >
                Add users
              </div>
              <div
                className={
                  selectedTab === "" ? "selectedoptions" : "options"
                }
                onClick={() => changeDisplay("Add Admin")}
              >
                Add admins
              </div>
              <div
                className={
                  selectedTab === "Performance Analysis"
                    ? "selectedoptions"
                    : "options"
                }
                onClick={() => changeDisplay("Performance Analysis")}
              >
                Performance Analysis
              </div>
              <div
                className={
                  selectedTab === "Message User"
                    ? "selectedoptions"
                    : "options"
                }
                onClick={() => changeDisplay("Message User")}
              >
                Message Users
              </div>
              <div
                className={
                  selectedTab === "Notifications"
                    ? "selectedoptions"
                    : "options"
                }
                onClick={() => changeDisplay("Notifications")}
              >
                Notifications
              </div>
              <div
                className="options"
    
                onClick={() => handleLogout()}
              >
                Logout
              </div>
            </div>
          </div>
          <div className="col-md-8">
            <div className="nav-bar">
              <div className="selected-tab">{selectedTab}</div>
            </div>
            <div className="main-body">
              {selectedTab === "Performance Analysis" ? (
                <Performance />
              ) : selectedTab === "Add Users" ? (
                <AddUser />
              ) : selectedTab === "Notifications" ? (
                <Notifications />
              ) : selectedTab === "Message User" ? (
                <MessageUser />
              ) : (
                <MainDashboard />
              )}
            </div>
          </div>
        </div>
        <AdminRoute />
      </>
    );

};

export default AdminLayout;
