// import * as actionType from './logType';
import apolloClient from "../../../config/apolloclient";
import gql from "graphql-tag";
import * as actions from "../../../store/actions";

export const getUsers = () => async (dispatch) => {
    let x = await apolloClient.mutate({
      mutation: gql`
        query MyQuery {
          users(where: { user_type: { _eq: "user" } }) {
            email
          }
        }
      `
    });
    return x;
};

export const getUsersHours = () => async (dispatch) => {
    let x = await apolloClient.mutate({
      mutation: gql`
        query MyQuery {
          time_tracker {
            time_tracked
          }
        }
      `,
    });
    return x;
};

export const signUpUser = (user) => async (dispatch) => { 
    try {
        let x = await apolloClient.mutate({
            mutation: gql`
        mutation MyMutation($email : String!, $password: String!) {
          insert_users(
            objects: { email: $email, password:$password , user_type: "user" }
          ) {
            affected_rows
            returning {
              email
            }
          }
        }
      `,
            variables: {
                email: user.email,
                password: user.password
            },
        });

        if (x) { 
             dispatch(
               actions.setToasterState(
                 true,
                 "Success",
                 "User Added ",
                 "User is created"
               )
             );
        }
        
    }
    catch (e) { 
        dispatch(
          actions.setToasterState(
            true,
            "Error",
            "Problem creating user ",
            "User wasn't created"
          )
        );
    }
}
