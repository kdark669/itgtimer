import React, { useState } from "react";
import { useDispatch } from "react-redux";
import * as actions from "./../actions/dashboardActions";

import Toaster from "../../../common/Toaster/Toaster";
const AddUser = (props) => {

const dispatch = useDispatch();
const [user, setUsers] = useState({});
    const handleChange = (e) => {
     console.log(e.target.value)
        e.persist()
        setUsers({
            ...user,
            [e.target.name]: e.target.value
        })

    }

     const handleSubmit = () => {
         dispatch(actions.signUpUser(user));
     };
  return (
    <>
          <div className="add-box">
              <Toaster/>
        <div className="selected-tab" style={{ textAlign: "center" }}>
          Enter the details for user
        </div>
        <div className="form-group">
          <input
            className="input-screen"
            type="email"
            name={"email"}
            placeholder={"Your email goes here"}
            onChange={(e) => handleChange(e)}
          />
        </div>
        <div className="form-group">
          <input
            className="input-screen"
            type="password"
            name={"password"}
            placeholder={"Your password here"}
            onChange={(e) => handleChange(e)}
          />
        </div>
        <div className="form-group">
          <div
            className="btn primary d-flex justify-evenly align-center"
            onClick={() => handleSubmit()}
          >
            <div>Create the user</div>
          </div>
              </div>
              
      </div>
    </>
  );
};

export default AddUser;
