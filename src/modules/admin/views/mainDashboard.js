import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import * as actions from "./../actions/dashboardActions";

const MainDashboard = (props) => {
  const [usersCount, setUsersCount] = useState("0");
  const [usersHours, setUsersHours] = useState("0");
  const dispatch = useDispatch();
  useEffect(() => {
    getUsersCount();
  }, []);

  const getUsersCount = async() => { 
    let x = await dispatch(actions.getUsers());
    setUsersCount(x.data.users.length);
    let time = await dispatch(actions.getUsersHours());
    let timeArray = [];
    time.data.time_tracker.map((t) => timeArray.push(t.time_tracked));
    let totalInSeconds = timeArray.reduce((a, b) => a + b, 0);
    setUsersHours(((totalInSeconds % 86400000) / 3600000).toFixed(2));
  }
  
  return (
    <>
      <div className="upper-dash">
        <div className="info-bar">
          <span>Total Users</span>
          <span style={{ fontSize: 60, marginTop: 30 }}>{usersCount}</span>
        </div>
        <div className="info-bar">
          <span>Total Users Hours</span>
          <span style={{ fontSize: 60, marginTop: 30 }}>
            {usersHours}
          </span>
        </div>
      </div>
    </>
  );
};

export default MainDashboard;
