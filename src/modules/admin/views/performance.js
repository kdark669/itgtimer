import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import * as actions from "./../actions/dashboardActions";

const Performance = (props) => {
    const [emails, setEmails] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    getUsersEmail();
  }, []);

  const getUsersEmail = async () => {
      let x = await dispatch(actions.getUsers());
      console.log(x.data.users);
      setEmails(x.data.users);
  };
    
    const checkuser = (email) => { 
        console.log(email);
    }

  return (
    <>
      <div className="upper-dash">
              <div style={{display: "flex", flexDirection: "column"}}>
          {emails.map((email, key) => (
            <div className="emails-row">
              <span> {`${key + 1} .${email.email}`}</span>
              <div className="analyze-link" onClick={() => checkuser(email)}>
                Analyze
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default Performance;
