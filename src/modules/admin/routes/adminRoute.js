import React from 'react';
import { Switch,Route } from 'react-router-dom'
import routeList from "./routeList";


const AdminRoute = () => {
    return (
        <React.Suspense fallback={<div>Loading</div>}>
            <Switch>
                {

                    routeList.map((route, key) => (
                        <Route path={`/admin${route.path}`} name={route.name} component={route.component} exact={route.exact} key={key} />
                    ))
                }
            </Switch>
        </React.Suspense>
    );
};

export default AdminRoute;
