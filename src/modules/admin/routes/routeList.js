import Dashboard from "../views/Dashboard";
import Profile from "../views/Profile";

export default [
    {
        path: "",
        name: "admin",
        component: Dashboard,
        exact: true,
    },
    {
        path: "/profile",
        name: "admin.profile",
        component: Profile,
        exact: true,
    },
]
