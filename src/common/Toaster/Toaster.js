import React, { useEffect } from "react";
import './Toaster.scss';
import * as actions from './services/ToasterActions'
import {useDispatch, useSelector} from "react-redux";

const Toaster = (props) => {
  const dispatch = useDispatch()
  const {
    title,
    name,
    message,
    appear
  } = useSelector(state => state.toasterReducer)

  useEffect(() => {
    const timer = setTimeout(() => {
      dispatch(actions.setToasterState(false, "", "",""))
    }, 3000);
    console.log("subas", title, name, message);
    return () => clearTimeout(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [appear]);

  return (
    <div
      className={`toaster ${
        appear ? "on" : "off"
      } ${title.toLowerCase()}`}
    >
      <div className="toaster-title bold">{name}</div>
      <div className="toaster-description">{message}</div>
    </div>
  );
};

export default Toaster;
