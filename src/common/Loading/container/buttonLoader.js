import React from 'react';

const ButtonLoader = () => {
    return (
        <div className="lds-ring-button">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
};

export default ButtonLoader;
