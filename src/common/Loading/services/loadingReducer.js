import * as actionTypes from './loadingType'

const initialState = {
    isLoading: false,
    loadingState: null
}

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.SET_LOADING:
            return {
                ...state,
                isLoading: true,
                loadingState: actions.loadingState && actions.loadingState,
            };
        case actionTypes.CLEAR_LOADING:
            return initialState
        default:
            return state
    }
}
