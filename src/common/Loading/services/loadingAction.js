import * as actionType from './loadingType'

export const setLoading = ( loadingState ) => {
    return {
        type: actionType.SET_LOADING,
        loadingState: loadingState
    }
}

export const clearLoading = () => {
    return {
        type: actionType.CLEAR_LOADING
    }
}
