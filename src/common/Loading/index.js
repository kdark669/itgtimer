import React from 'react';
import './loading.scss'
import {useSelector} from "react-redux";
import ButtonLoader from "./container/buttonLoader";
import InlineLoader from "./container/inlineLoader";

const Loading = props => {
    const {
        isButton
    } = props
    const {isLoading} = useSelector(state => state.loadingReducer)
    return (
        <>
            {
                isLoading && (
                    <div className="loader-container">
                        {
                            isButton ? <ButtonLoader/> : <InlineLoader/>
                        }
                    </div>
                )
            }
        </>
    );
};

export default Loading;
