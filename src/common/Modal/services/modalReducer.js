import * as actionTypes from './modalType'

const initialState = {
    isOpenModal: false,
    mode:null,
}

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.OPEN_MODAL:
            return {
                ...state,
                isOpenModal: true,
                mode: actions.mode && actions.mode,
            };
        case actionTypes.CLOSE_MODAL:
            return {
                ...state,
                mode:null,
                isOpenModal: false
            };
        default:
            return state
    }
}
