import React from 'react';
import * as actions from './services/modalAction'
import './modal.scss'
import {useDispatch, useSelector} from "react-redux";

const Modal = props => {

    const dispatch = useDispatch()
    const { isOpenModal } = useSelector(state => state.modalReducer)

    const { children } = props;
    return (
        <>
            {
                isOpenModal && (
                    <div className="modal">
                        <div className="modal-backdrop" onClick={() => {
                            dispatch(actions.closeModal())
                        }}/>
                        <div className="modal-card">
                            <div className="modal-area">
                                { children }
                            </div>
                        </div>
                    </div >
                )

            }
        </>
    )
};
export default Modal;
