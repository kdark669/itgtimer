import React, {useEffect} from 'react';
import { useSelector} from "react-redux";
// import * as actions from '../store/actions'
import {Route, Redirect} from 'react-router-dom'

const PrivateRoute = ({component: Component, setToasterState, ...rest}) => {
    // const dispatch = useDispatch()

    const {isLoggedIn} = useSelector(state => state.authenticationReducer)

    const openToaster = () => {
        // dispatch(actions.setToasterState(
        //     true,
        //     "error",
        //     "Authentication Error",
        //     "You have to authenticate before proceedings "
        // ))
    }

    useEffect(() => {
        !isLoggedIn && openToaster()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoggedIn])

    return (
        <Route {...rest} render={
            props => !isLoggedIn ? (<Redirect to="/login"/>) :
                (<Component {...props} />)
        }
        />
    );
};

export default PrivateRoute
