import AdminLayout from '../modules/admin';
// import LandingLayout from '../modules/landing';
import Login from '../modules/Authentication/container/login';
import LogForm from '../modules/landing/component/LogForm';
// import { FALSE } from 'node-sass';

export default [
  {
    path: "/admin",
    name: "admin",
    component: Login,
    isAuth: false,
    exact: false,
  },
  {
    path: "/adminDash",
    name: "dashboard",
    component: AdminLayout,
    isAuth: true,
    exact: true,
  },
  {
    path: "/",
    name: "landing",
    component: Login,
    isAuth: true,
    exact: true,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    isAuth: false,
    exact: true,
  },
  {
    path: "/logform",
    name: "LogForm",
    component:  LogForm  ,
    isAuth: true,
  },
];
