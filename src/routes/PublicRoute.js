import React from 'react';
import { useSelector} from "react-redux";
import {Route, Redirect} from 'react-router-dom'

const PublicRoute = ({component: Component, setToasterState, ...rest}) => {

    const { isLoggedIn } = useSelector(state => state.authenticationReducer)
    const isAdmin = rest.path === "/admin" || "/adminDash";
    return (
      <Route
        {...rest}
        render={(props) =>
          isLoggedIn ? (
            isAdmin ? (
              <Redirect to="/adminDash" />
            ) : (
              <Redirect to="/logform" />
            )
          ) : (
            <Component {...props} />
          )
        }
      />
    );
};

export default PublicRoute
